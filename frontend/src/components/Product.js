import React from 'react';
import { Link } from 'react-router-dom';

export default function Product(props){
    const {product} = props;

    return(
        
            <div key={product._id} className="card">
        <Link to={`/product/${product._id}`}>
                    <img className="medium" src={product.image} alt={product.name}/>
                <div className="card-body">
                    <p>{product.name}</p>
                    <div><span className="peso">₱</span><span className="price">{product.price}</span><span className="sold">
                        {product.sold>=1000
                        ?Math.round((product.sold/1000)*10)/10+"K"
                        :product.sold} sold</span></div>
                </div>
        </Link>
                <Link to={`/match/${product.category}`} ><div className="find-similar">Find Similar</div></Link>  
            </div>
       
    )

}