import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { signin } from '../actions/userActions.js';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

export default function Login(props) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const redirect = props.location.search
    ? props.location.search.split('=')[1]
    : '/';

  const userSignin = useSelector((state) => state.userSignin);
  // const userSignin = useSelector((state) => state.userSignin);
  const { userInfo, loading, error } = userSignin;

  const dispatch = useDispatch();

  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(signin(email, password));
    // window.location.href = "/";
  };

  useEffect(() => {
    if (userInfo) {
      props.history.push(redirect);
    }
  }, [props.history, redirect, userInfo]);

  return (
    <div className="container-login">
    <div className="image-login">
    <img src="../images/loginbg.png" />
    </div>
    <div className="form-login">
      <form className="form" onSubmit={submitHandler}>
        <div>
          <span className="lu-login">Log In</span>
        </div>
        {loading && <LoadingBox></LoadingBox>}
        {error && <MessageBox variant="danger">{error}</MessageBox>}
        <div>
          <input
            type="email"
            id="email"
            placeholder="Email"
            required
            onChange={(e) => setEmail(e.target.value)}
          ></input>
        </div>
        <div>
          <input
            type="password"
            id="password"
            placeholder="Password"
            required
            onChange={(e) => setPassword(e.target.value)}
          ></input>
        </div>
        <div>
          <label />
          <button className="btn-login" type="submit">
            LOG IN
          </button>
        </div>
        <div>
          <label />
          <hr />
          <div className="reg-login">
            <span className="nts-login">New to Shopee?</span> <Link to={`/register?redirect=${redirect}`}><span className="su-login">Sign Up</span></Link>
          </div>
        </div>
      </form>
    </div>
    </div>
  );
}