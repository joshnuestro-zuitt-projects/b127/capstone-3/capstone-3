import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { addToCart, removeFromCart } from '../actions/cartActions';
import MessageBox from '../components/MessageBox';

export default function Cart(props) {
  const productId = props.match.params.id;
  const qty = props.location.search
    ? Number(props.location.search.split('=')[1])
    : 1;

    const cart = useSelector(state => state.cart);
    const {cartItems} = cart;
    const dispatch = useDispatch();
    useEffect(()=>{
        if(productId){
            dispatch(addToCart(productId, qty));
        }
    },[dispatch, productId, qty])

  const removeFromCartHandler = (id) =>{
    dispatch(removeFromCart(id));
  }

  const checkoutHandler = () => {
    props.history.push('/signin?redirect=shipping');
  }

  return (
    <div>
      <div className="center">
        <h1>Shopping Cart</h1>
        {cartItems.length === 0 ? (
          <MessageBox>
            Cart is empty. <Link to="/">Go Shopping</Link>
          </MessageBox>
        ) : (
          <ul>
            {cartItems.map((item) => (
              <li key={item.product}>
                <div className="row">
                  <div>
                    <img
                      src={item.image}
                      alt={item.name}
                      className="small"
                    ></img>
                  </div>
                  <div className="min-30">
                    <Link to={`/product/${item.product}`}>{item.name}</Link>
                  </div>
                  <div>
                    <span className="slashed-price2">₱{Math.round(item.price*1.3)}</span>
                    <span className="new-price2">₱{item.price}</span>
                  </div>
                  <div>
                    {/* <select
                      value={item.qty}
                      onChange={(e) =>
                        dispatch(
                          addToCart(item.product, Number(e.target.value))
                        )
                      }
                    >
                      {[...Array(item.stock).keys()].map((x) => (
                        <option key={x + 1} value={x + 1}>
                          {x + 1}
                        </option>
                      ))}
                    </select>
                     */}
                    <button className="btn-add-item"
                      onClick={(e) => 
                          item.qty>1
                          ? dispatch(
                            addToCart(item.product, Number(item.qty-1))
                          )
                          : dispatch(
                            addToCart(item.product, Number(1))
                          )
                      }
                      >-</button>

                      <button className="num-item" disabled>{item.qty}</button>

                      <button className="btn-add-item"
                      onClick={(e) => 
                        item.qty>=item.stock
                        ? dispatch(
                        addToCart(item.product, Number(item.stock))
                      )
                        : dispatch(
                          addToCart(item.product, Number(item.qty+1))
                        )
                    }
                      >+</button>

                  </div>
                  <div className="total-price">₱{item.price*item.qty}</div>
                  <div>
                    <button
                      className="btn-save"
                      type="button"
                      onClick={() => removeFromCartHandler(item.product)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </li>
            ))}
          </ul>
        )}
      </div>

        <div className="center"> 

          <div className="to-table">

            <div className="to-row">

                <span className="label-total-cart to-cell">Total ({cartItems.reduce((a, c) => a + c.qty, 0)} items):</span>
                <span className="total-cart to-cell">₱{cartItems.reduce((a, c) => a + c.price * c.qty, 0)}</span>


              <button
                type="button"
                onClick={checkoutHandler}
                className="btn-checkout to-cell"
                disabled={cartItems.length === 0}
              >
                Checkout
              </button>

            </div>

            <div className="to-row">
              
  
              <span className="label-saved-cart to-cell">Saved</span> 
              <span className="saved-cart to-cell">₱{Math.round(cartItems.reduce((a, c) => a + (c.price * c.qty * 1.3) -  (c.price * c.qty), 0))}</span>
        

            </div>

          </div>

          {/* <ul>
            <li>
              <h2>
                Total ({cartItems.reduce((a, c) => a + c.qty, 0)} items): ₱
                {cartItems.reduce((a, c) => a + c.price * c.qty, 0)}
              </h2>
            </li>
            <li>
              <h2>Saved {Math.round(cartItems.reduce((a, c) => a + (c.price * c.qty * 1.3) -  (c.price * c.qty), 0))}
              </h2>
            </li>
            <li>
              <button
                type="button"
                onClick={checkoutHandler}
                className="btn-save"
                disabled={cartItems.length === 0}
              >
                Checkout
              </button>
            </li>
          </ul> */}
        </div>

    </div>
  );
}