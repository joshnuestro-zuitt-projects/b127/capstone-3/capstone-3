import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { savePaymentMethod } from '../actions/cartActions';
import CheckoutSteps from '../components/CheckoutSteps';

export default function PaymentMethod(props) {
  const cart = useSelector((state) => state.cart);
  const { shippingAddress } = cart;
  if (!shippingAddress.address) {
    props.history.push('/shipping');
  }
  const [paymentMethod, setPaymentMethod] = useState('UnionBank');
  const dispatch = useDispatch();

  console.log(localStorage)

//   let address = "";

//   (localStorage.shippingAddress != undefined)
//   ? address = (JSON.parse(localStorage.shippingAddress))
//   : console.log(localStorage.userInfo)


  const submitHandler = (e) => {
    e.preventDefault();
    dispatch(savePaymentMethod(paymentMethod));
    props.history.push('/placeorder');
  };

  return (
    <div>
      <CheckoutSteps step1 step2 step3></CheckoutSteps>
        
        {/* <div className="container-payment">
            <div className="deladd-payment">Delivery Address</div>
            <div>
                <span className="name-payment">{`${address.fullName}`}</span> 
                <span className="address-payment">{`${address.address}, ${address.city}, ${address.postalCode}`}</span>
            </div>
        </div> */}

      <form className="form" onSubmit={submitHandler}>
        {/* <hr /> */}
        <div className="container-payment">
          <h1>Payment Method</h1>
        </div>
        <hr />
        <div className="container-payment">
        <div>
          <div>
            <input
              type="radio"
              id="unionbank"
              value="UnionBank"
              name="paymentMethod"
              required
              checked
              onChange={(e) => setPaymentMethod(e.target.value)}
            ></input>
            <label htmlFor="unionbank">Unionbank Internet Banking</label>
          </div>
        </div>
        <div>
          <div>
            <input
              type="radio"
              id="rcbc"
              value="RCBC Access One"
              name="paymentMethod"
              required
              onChange={(e) => setPaymentMethod(e.target.value)}
            ></input>
            <label htmlFor="rcbc">RCBC AccessOne</label>
          </div>
        </div>
        <div>
          <div>
            <input
              type="radio"
              id="metrobank"
              value="Metrobankdirect"
              name="paymentMethod"
              required
              onChange={(e) => setPaymentMethod(e.target.value)}
            ></input>
            <label htmlFor="metrobank">Metrobankdirect</label>
          </div>
        </div>
        <div>
          <label />
          <button className="btn-payment" type="submit">
            Continue
          </button>
        </div>
        </div>
      </form>
    </div>
  );
}