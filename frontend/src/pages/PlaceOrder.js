import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createOrder } from '../actions/orderActions';
import CheckoutSteps from '../components/CheckoutSteps';
import { ORDER_CREATE_RESET } from '../constants/orderConstants';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

export default function PlaceOrder(props) {
  const cart = useSelector((state) => state.cart);
  if (!cart.paymentMethod) {
    props.history.push('/payment');
  }
  const orderCreate = useSelector((state) => state.orderCreate);
  const { loading, success, error, order } = orderCreate;
  const toPrice = (num) => Number(num.toFixed(2)); // 5.123 => "5.12" => 5.12
  cart.itemsPrice = toPrice(
    cart.cartItems.reduce((a, c) => a + c.qty * c.price, 0)
  );
  cart.shippingPrice = cart.itemsPrice > 100 ? toPrice(0) : toPrice(10);
  cart.totalPrice = cart.itemsPrice + cart.shippingPrice;
  const dispatch = useDispatch();

  const placeOrderHandler = () => {
    dispatch(createOrder({ ...cart, orderItems: cart.cartItems }));
  };

  useEffect(() => {
    if (success) {
      props.history.push(`/order/${order._id}`);
      dispatch({ type: ORDER_CREATE_RESET });
    }
  }, [dispatch, order, props.history, success]);

  return (
    <div>
      <CheckoutSteps step1 step2 step3 step4></CheckoutSteps>

        <div className="col-2">

              <div className="container-payment">
              <div className="deladd-payment">Delivery Address</div>

                <span className="name-payment">{cart.shippingAddress.fullName}</span>
                <span className="address-payment"> {cart.shippingAddress.address},
                  {cart.shippingAddress.city}, {cart.shippingAddress.postalCode}</span>
 
              </div>
           
            <div className="card card-body">
            <div className="deladd-payment">Products Ordered</div>
                <ul>
                  {cart.cartItems.map((item) => (
                    <li key={item.product}>
                      <div className="row">
                        <div>
                          <img
                            src={item.image}
                            alt={item.name}
                            className="small"
                          ></img>
                        </div>
                        <div className="min-30">
                          {/* <Link to={`/product/${item.product}`}>
                            {item.name}
                          </Link> */}
                          {item.name}
                        </div>

                        <div>₱{item.price}</div>

                        <div>{item.qty}</div>
                            
                        <div>₱{item.qty * item.price}</div>

                      </div>
                    </li>
                  ))}
                </ul>
              </div>



              <div className="container-payment">
              <div className="deladd-payment">Payment Method</div>
 
                  {cart.paymentMethod}
          
              </div>     

        </div>
        <div>
          <div className="container-payment">
                <div className="row">
                  <div>Merchandise Subtotal:</div>
                  <div>₱{cart.itemsPrice.toFixed(2)}</div>
                </div>
                <div className="row">
                  <div>Shipping Total:</div>
                  <div>₱{cart.shippingPrice.toFixed(2)}</div>
                </div>
                <div className="row">
                <div>Total Payment:</div>
                  <div>
                    <strong>₱{cart.totalPrice.toFixed(2)}</strong>
                  </div>
                </div>
                <button
                  type="button"
                  onClick={placeOrderHandler}
                  className="primary block"
                  disabled={cart.cartItems.length === 0}
                >
                  Place Order
                </button>
          {loading && <LoadingBox></LoadingBox>}
          {error && <MessageBox variant="danger">{error}</MessageBox>}
          </div>

        </div>

    </div>
  );
}