import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { detailsProduct } from '../actions/productActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';
import Rating from '../components/Rating';

export default function Item(props){
    const dispatch = useDispatch();
    const productId = props.match.params.id;
    const [qty, setQty] = useState(1);
    const productDetails = useSelector((state) => state.productDetails);
    const { loading, error, product } = productDetails;
    
     useEffect(() => {
       dispatch(detailsProduct(productId));
     }, [dispatch, productId]);

     const addToCartHandler = () => {
        props.history.push(`/cart/${productId}?qty=${qty}`);
      };
    
    // console.log(JSON.parse(localStorage.userInfo)); //to show userInfo in JSON
    let isAdmin = false;

    (localStorage.userInfo != undefined)
    ? isAdmin = (JSON.parse(localStorage.userInfo)).isAdmin
    : console.log(localStorage.userInfo)

    return (
    <div>
        {loading
        ? (<LoadingBox></LoadingBox>)
        : error
        ? (<MessageBox variant="danger">{error}</MessageBox>)
        : (
            <div>
                <div><Link to="/">Shopee</Link> &gt; {product.category} &gt; {product.name}</div>
                <div className="row top">
                    <div className="col-pic">
                        <img className="large" src={product.image} alt={product.name}></img>
                    </div>
                    <div className="col-desc">

                            <h1>{product.name}</h1>
                            
                            <Rating rating={product.rating} reviewers={product.reviewers}></Rating>
                                
                            <span className="num-sold">
                                {product.sold>=1000
                                ?Math.round((product.sold/1000)*10)/10+"K"
                                :product.sold}
                            </span>
                            <span className="word-sold"> 
                                Sold
                            </span>
                                
                            <div className="item-price">
                                <div>
                                    <span className="slashed-price">₱{Math.round(product.price*1.3)}</span>
                                    <span className="new-price">₱{product.price}</span>
                                </div>
                                <div><img src="../images/lpg.png" alt="lowest price guaranteed"></img></div>
                            </div>

                            <hr />
                            
                            <div className="desc-item">
                                Product Specifications
                            </div>

                            <div className="to-table">

                                <div className="to-row">
                                    <div className="to-cell desc-left">Category</div>
                                    <div className="to-cell desc-right"><Link to="/">Shopee</Link> &gt; {product.category}</div>
                                </div>
                                <div className="to-row">
                                    <div className="to-cell desc-left">Brand</div>
                                    <div className="to-cell desc-right">{product.brand}</div>
                                </div>
                                <div className="to-row">
                                    <div className="to-cell desc-left">Stock</div>
                                    <div className="to-cell desc-right">{product.stock}</div>
                                </div>
                            
                            </div>



                            <hr />

                            <div className="desc-item">
                                Product Description
                            </div>
                            <div>
                                <span className="desc-right">{product.description}</span>

                                {product.stock>0 && isAdmin == false
                                 ?(
                                    <>
                                    <div className="add-item">
                                        <span className="qty-item">Quantity</span>
                                        <span>
                                            {/* <select
                                            value={qty}
                                            onChange={(e) => setQty(e.target.value)}
                                            >
                                            {
                                            [...Array(product.stock).keys()].map(
                                                (x) => (
                                                <option key={x + 1} value={x + 1}>
                                                    {x + 1}
                                                </option>
                                                )
                                            )
                                            }
                                            </select> */}

                                            
                                            <button className="btn-add-item"
                                            onClick={(e) => 
                                                qty>1
                                                ? setQty(qty-1)
                                                : setQty(1)
                                            }
                                            >-</button>

                                            <button className="num-item" disabled>{qty}</button>

                                            <button className="btn-add-item"
                                            onClick={(e) => 
                                                qty>=product.stock
                                                ? setQty(product.stock)
                                                : setQty(qty+1)}
                                            >+</button>

                                    <span className="qty-item">
                                        {product.stock>0 &&
                                        (<><span className="success">{product.stock} piece available</span>
                                        <div><button onClick={addToCartHandler} className="lee-min-ho">🛒 Add to Cart</button></div></>)
                                        }
                                    </span>
                                            
                                        </span>
                                        
                                    </div>
                                        
                                    </>
                                )
                                : isAdmin == false && <div className="danger">Unavailable</div>
                                }
                                    
   
                                    
                                        
                            </div>
                    </div>
                </div>
            </div>
        )}
    </div>
    )

}