import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { detailsOrder } from '../actions/orderActions';
import LoadingBox from '../components/LoadingBox';
import MessageBox from '../components/MessageBox';

export default function Order(props) {
  const orderId = props.match.params.id;
  const orderDetails = useSelector((state) => state.orderDetails);
  const { order, loading, error } = orderDetails;
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(detailsOrder(orderId));
  }, [dispatch, orderId]);
  return loading ? (
    <LoadingBox></LoadingBox>
  ) : error ? (
    <MessageBox variant="danger">{error}</MessageBox>
  ) : (
    <div>
    <h1>Order {order._id}</h1>

      <div className="col-2">

            <div className="container-payment">
            <div className="deladd-payment">Delivery Address</div>

              <span className="name-payment">{order.shippingAddress.fullName}</span>
              <span className="address-payment"> {order.shippingAddress.address},
                {order.shippingAddress.city}, {order.shippingAddress.postalCode}</span>

                {order.isDelivered ? (
                  <MessageBox variant="success">
                    Delivered at {order.deliveredAt}
                  </MessageBox>
                ) : (
                  <MessageBox variant="danger">Not Delivered</MessageBox>
                )}
            </div>
         
          <div className="card card-body">
          <div className="deladd-payment">Products Ordered</div>
              <ul>
                {order.orderItems.map((item) => (
                  <li key={item.product}>
                    <div className="row">
                      <div>
                        <img
                          src={item.image}
                          alt={item.name}
                          className="small"
                        ></img>
                      </div>
                      <div className="min-30">
                        {/* <Link to={`/product/${item.product}`}>
                          {item.name}
                        </Link> */}
                        {item.name}
                      </div>

                      <div>₱{item.price}</div>

                      <div>{item.qty}</div>
                          
                      <div>₱{item.qty * item.price}</div>

                    </div>
                  </li>
                ))}
              </ul>
            </div>


            <div className="container-payment">
            <div className="deladd-payment">Payment Method</div>

                {order.paymentMethod}

                {order.isPaid ? (
                  <MessageBox variant="success">
                    Paid at {order.paidAt}
                  </MessageBox>
                ) : (
                  <MessageBox variant="danger">Not Paid</MessageBox>
                )}        
            </div>     

      </div>
      <div>
        <div className="container-payment">
              <div className="row">
                <div>Merchandise Subtotal:</div>
                <div>₱{order.itemsPrice.toFixed(2)}</div>
              </div>
              <div className="row">
                <div>Shipping Total:</div>
                <div>₱{order.shippingPrice.toFixed(2)}</div>
              </div>
              <div className="row">
              <div>Total Payment:</div>
                <div>
                  <strong>₱{order.totalPrice.toFixed(2)}</strong>
                </div>
              </div>

        </div>

      </div>

  </div>
  );
}