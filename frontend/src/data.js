import bcrypt from 'bcryptjs';

const data = {
    users: [
        {
            name: 'Joshua',
            email: 'admin@mail.com',
            password: bcrypt.hashSync('admin', 8),
            isAdmin: true
        },
        {
            name: 'Generic User',
            email: 'user@mail.com',
            password: bcrypt.hashSync('user', 8),
            isAdmin: false
        }
    ],
    products: [
        {
            _id:'1',
            name: 'Cat Litter 1',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '../images/product-4.jpg',
            price: 240,
            stock: 3520,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1200
        },
        {
            _id:'2',
            name: 'Lint Remover',
            description: 'No more achoo',
            category: 'Pet Care',
            image: '/images/product-2.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 300
        },
        {
            _id:'3',
            name: 'Pet Paw Hair Trimmer',
            description: 'Di na siya madudulas',
            category: 'Pet Care',
            image: '/images/product-3.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 250
        },
        {
            _id:'4',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 3.5,
            reviewers: 5360,
            sold: 1365
        },
        {
            _id:'5',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 3,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'6',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'7',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'8',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'9',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'10',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'11',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        },
        {
            _id:'12',
            name: 'Cat Litter',
            description: 'Tight clumping formula',
            category: 'Pet Care',
            image: '/images/product-1.jpg',
            price: 240,
            brand: 'Cool Clean',
            rating: 4.5,
            reviewers: 5,
            sold: 1000
        }
    ]
}

export default data;